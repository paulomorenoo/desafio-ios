//
//  RepositoryCell.swift
//  Gitlr
//
//  Created by Paulo Moreno on 26/08/16.
//  Copyright © 2016 Paulo Moreno. All rights reserved.
//

import UIKit

class RepositoryCell: UITableViewCell {

    @IBOutlet weak var lblRepoName: UILabel!
    @IBOutlet weak var lblRepoDescription: UILabel!
    @IBOutlet weak var lblRepoNumForks: UILabel!
    @IBOutlet weak var lblRepoNumStars: UILabel!
    
    @IBOutlet weak var imgUserPicture: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    
    
}
