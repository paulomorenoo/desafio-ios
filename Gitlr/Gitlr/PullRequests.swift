//
//  Repository.swift
//  Gitlr
//
//  Created by Paulo Moreno on 26/08/16.
//  Copyright © 2016 Paulo Moreno. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

// Enum containing fields name from the GitHub api
enum PRFields: String{
    case Id = "id"
    case Title = "title"
    case Date = "created_at"
    case Body = "body"
    case Author = "user"
    case AuthorName = "login"
    case authorImageURL = "avatar_url"
    case Url = "html_url"
    
    case WrapperCount = "total_count"
    case WrapperPRs = "items"
}

// Wrapper object that will keep the full response from the API call
class PullRequestWrapper {
    var PullRequests: [PullRequest]?
    var count: Int?
}

// Main repository object
class PullRequest{
    
    var id: Int?
    var title: String?
    var body: String?
    var date: NSDate?
    var authorName: String?
    var authorImageURL: String?
    var url: String?
    
    required init(json: JSON){
        let dateFormatter = NSDateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZ"
        
        self.id = json[PRFields.Id.rawValue].int
        self.title = json[PRFields.Title.rawValue].stringValue
        self.body = json[PRFields.Body.rawValue].stringValue
        self.date = dateFormatter.dateFromString(json[PRFields.Date.rawValue].string!)
        self.authorName = json[PRFields.Author.rawValue][PRFields.AuthorName.rawValue].stringValue
        self.authorImageURL = json[PRFields.Author.rawValue][PRFields.authorImageURL.rawValue].stringValue
        self.url = json[PRFields.Url.rawValue].stringValue
    }
    
    // Get all pull requests given a URL
    class func getPullRequests(path: String, completionHandler: (PullRequestWrapper?, NSError?) -> Void) {
        Alamofire.request(.GET, path)
            .responsePullRequestsArray { response in
                completionHandler(response.result.value, response.result.error)
        }
    }
}

//Create custom response serializer to get the pull requests in a wrapper object
extension Alamofire.Request {
    func responsePullRequestsArray(completionHandler: Response<PullRequestWrapper, NSError> -> Void) -> Self {
        let responseSerializer = ResponseSerializer<PullRequestWrapper, NSError> { request, response, data, error in
            guard error == nil else {
                return .Failure(error!)
            }
            guard let responseData = data else {
                let failureReason = "Array could not be serialized because input data was nil."
                let error = Error.errorWithCode(.DataSerializationFailed, failureReason: failureReason)
                return .Failure(error)
            }
            
            let JSONResponseSerializer = Request.JSONResponseSerializer(options: .AllowFragments)
            let result = JSONResponseSerializer.serializeResponse(request, response, responseData, error)
            
            switch result {
            case .Success(let value):
                // Get the wrapper object from json
                
                let json = SwiftyJSON.JSON(value)
                let wrapper = PullRequestWrapper()
                
                // Set the number of pull requests
                wrapper.count = json.array?.count

                // get each pull request and put on the wrapper
                var allPullRequests = [PullRequest]()
                for jsonPullRequest in json
                {
                    let pr = PullRequest(json: jsonPullRequest.1)
                    allPullRequests.append(pr)
                }
                wrapper.PullRequests = allPullRequests
                return .Success(wrapper)
            case .Failure(let error):
                return .Failure(error)
            }
        }
        
        return response(responseSerializer: responseSerializer,
                        completionHandler: completionHandler)
    }
}