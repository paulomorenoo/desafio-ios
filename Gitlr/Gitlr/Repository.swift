//
//  Repository.swift
//  Gitlr
//
//  Created by Paulo Moreno on 25/08/16.
//  Copyright © 2016 Paulo Moreno. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

// Enum containing fields name from the GitHub api
enum RepFields: String{
    case Id = "id"
    case Name = "name"
    case Description = "description"
    case Owner = "owner"
    case OwnerName = "login"
    case OwnerImageURL = "avatar_url"
    case NumberStars = "stargazers_count"
    case NumberForks = "forks"
    
    case WrapperCount = "total_count"
    case WrapperRepositories = "items"
}

// Wrapper object that will keep the full response from the API call
class RepositoryWrapper {
    var repositories: [Repository]?
    var count: Int?
    
    var page: Int?
    var next: String?
}

// Main repository object
class Repository{
    
    var id: Int?
    var name: String?
    var description: String?
    var ownerName: String?
    var ownerImageURL: String?
    var numberStars: Int?
    var numberForks: Int?
    
    required init(json: JSON){
        self.id = json[RepFields.Id.rawValue].int
        self.name = json[RepFields.Name.rawValue].stringValue
        self.description = json[RepFields.Description.rawValue].stringValue
        self.ownerName = json[RepFields.Owner.rawValue][RepFields.OwnerName.rawValue].stringValue
        self.ownerImageURL = json[RepFields.Owner.rawValue][RepFields.OwnerImageURL.rawValue].stringValue
        self.numberStars = json[RepFields.NumberStars.rawValue].int
        self.numberForks = json[RepFields.NumberForks.rawValue].int
    }

    // MARK: Endpoints
    // Default endpoint (first page)
    class func endpointForRepository() -> String {
        return Repository.endpointForRepository("1")
    }
    
    // Get endpoint according to page
    class func endpointForRepository(page: String) -> String {
        return "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=" + page
    }
    
    // Get the repositories given a page
    private class func getRepositoriesAtPath(path: String, completionHandler: (RepositoryWrapper?, NSError?) -> Void) {
        // iOS 9: Replace HTTP with HTTPS
        // We dont need this, since all GitHub URLs are HTTPS
        //let securePath = path.stringByReplacingOccurrencesOfString("http://", withString: "https://", options: .AnchoredSearch)
        Alamofire.request(.GET, path)
            .responseRepositoriesArray { response in
                completionHandler(response.result.value, response.result.error)
        }
    }
    
    // To get the first page of repositores
    class func getRepository(completionHandler: (RepositoryWrapper?, NSError?) -> Void) {
        getRepositoriesAtPath(Repository.endpointForRepository(), completionHandler: completionHandler)
    }
    
    // Get more repositories (considering we have a 'next' url on the wrapper object)
    class func getMoreRepositories(wrapper: RepositoryWrapper?, completionHandler: (RepositoryWrapper?, NSError?) -> Void) {
        guard let nextPath = wrapper?.next else {
            completionHandler(nil, nil)
            return
        }
        getRepositoriesAtPath(nextPath, completionHandler: completionHandler)
    }
    
}

//Create custom response serializer to get the repositories in a wrapper object
extension Alamofire.Request {
    func responseRepositoriesArray(completionHandler: Response<RepositoryWrapper, NSError> -> Void) -> Self {
        let responseSerializer = ResponseSerializer<RepositoryWrapper, NSError> { request, response, data, error in
            guard error == nil else {
                return .Failure(error!)
            }
            guard let responseData = data else {
                let failureReason = "Array could not be serialized because input data was nil."
                let error = Error.errorWithCode(.DataSerializationFailed, failureReason: failureReason)
                
                return .Failure(error)
            }
            
            let JSONResponseSerializer = Request.JSONResponseSerializer(options: .AllowFragments)
            let result = JSONResponseSerializer.serializeResponse(request, response, responseData, error)
            
            switch result {
            case .Success(let value):
                let json = SwiftyJSON.JSON(value)
                let wrapper = RepositoryWrapper()

                // Get current page number
                let urlComponents = NSURLComponents(string: (request?.URLString)!)
                let queryItems = urlComponents?.queryItems
                let page = Int((queryItems?.filter({$0.name == "page"}).first?.value)!)
                
                // Set page number on wrapper
                wrapper.page = page
                // Set the 'next' page url
                wrapper.next = Repository.endpointForRepository(String(page!+1))
                //Get total amount of reps
                wrapper.count = json[RepFields.WrapperCount.rawValue].intValue
                
                // For each item on the repositories list, add it to the wrapper as a Repository object
                var allRepositories = [Repository]()
                let results = json[RepFields.WrapperRepositories.rawValue]
                for jsonRepository in results
                {
                    let rep = Repository(json: jsonRepository.1)
                    allRepositories.append(rep)
                }
                wrapper.repositories = allRepositories
                return .Success(wrapper)
            case .Failure(let error):
                return .Failure(error)
            }
        }
        
        return response(responseSerializer: responseSerializer,
                        completionHandler: completionHandler)
    }
}