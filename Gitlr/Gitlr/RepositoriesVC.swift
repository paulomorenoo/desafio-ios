//
//  RepositoriesVC.swift
//  Gitlr
//
//  Created by Paulo Moreno on 25/08/16.
//  Copyright © 2016 Paulo Moreno. All rights reserved.
//

import UIKit
import Alamofire

/* TableView controller class for displaying repositories
 */
class RepositoriesVC: UITableViewController {
    /* We have repositories (local array of repositories - that will be displayed on the table)
     * repositoryWrapper - object that represents the JSON response for a given page. We will use it to load new pages, and then add this
     * page's repositories to our local array of repositories
     * 
     * The flag is to avoid loading multiple times the same page
     */
    var repositories:Array<Repository>?
    var repositoryWrapper:RepositoryWrapper?
    var isLoadingRepositories = false
    
    @IBOutlet weak var tableview: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // On the first time, load the first page of repositoriess
        self.loadFirstRepositories()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let repositories = self.repositories else {
            return 0
        }
        return repositories.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // Custom cell for repositories
        let cell = tableView.dequeueReusableCellWithIdentifier("RepositoryCell", forIndexPath: indexPath) as! RepositoryCell
        
        if let numberOfRepositories = self.repositories?.count where numberOfRepositories >= indexPath.row {
            if let repository = self.repositories?[indexPath.row], let numberForks = repository.numberForks, numberStars = repository.numberStars {
        
                // Set cell attributes
                cell.lblRepoName.text = repository.name
                cell.lblRepoDescription.text = repository.description
                cell.lblRepoNumForks.text = "\(numberForks)"
                cell.lblRepoNumStars.text = "\(numberStars)"
                cell.lblUsername.text = repository.ownerName
                
                // Get the image asynchronous
                if let imgURL = repository.ownerImageURL{
                    Alamofire.request(.GET, imgURL).response { (request, response, data, error) in
                        cell.imgUserPicture.image = UIImage(data: data!, scale:1)
                    }
                }
                
            }
            
            // See if we need to load more repositories
            let rowsToLoadFromBottom = 5;
            if !self.isLoadingRepositories && indexPath.row >= (numberOfRepositories - rowsToLoadFromBottom) {
                if let totalRepositoriesCount = self.repositoryWrapper?.count where totalRepositoriesCount - numberOfRepositories > 0 {
                    self.loadMoreRepositories()
                }
            }
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        // On click - performe segue to display pull requests table
        performSegueWithIdentifier("showPullRequestsSegue", sender: self)
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)

    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showPullRequestsSegue" {
            
            if let indexPath = tableView.indexPathForSelectedRow, let repository = self.repositories?[indexPath.row], let owner = repository.ownerName, let repoName = repository.name   {
                
                // Get View Controller
                let pullRequestsVC = segue.destinationViewController as! PullRequestsVC
                
                // Set the URL (to load all Pull Requests)
                pullRequestsVC.pullRequestsURL = "https://api.github.com/repos/\(owner)/\(repoName)/pulls"
                // Updates title
                pullRequestsVC.title = repoName
                
                
            }
            
        }
    }
    
    /* Load the first page of repositories
     */
    func loadFirstRepositories() {
        isLoadingRepositories = true
        Repository.getRepository{ wrapper, error in
            guard error == nil else {
                // TODO: improved error handling
                self.isLoadingRepositories = false
                let alert = UIAlertController(title: "Error",
                    message: "Could not load first repositories \(error?.localizedDescription)",
                    preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                return
            }
            self.addRepositoriesFromWrapper(wrapper)
            self.isLoadingRepositories = false
            self.tableview?.reloadData()
        }
    }
    
    /* Load the next page of repositories
     */
    func loadMoreRepositories()
    {
        self.isLoadingRepositories = true
        guard let repositories = self.repositories, let wrapper = self.repositoryWrapper where repositories.count < wrapper.count else {
            // no more repositories to fetch
            return
        }
        // there are more species out there!
        Repository.getMoreRepositories(self.repositoryWrapper, completionHandler: { (moreWrapper, error) in
            guard error == nil else {
                // TODO: improved error handling
                self.isLoadingRepositories = false
                let alert = UIAlertController(title: "Error",
                    message: "Could not load more repositories \(error?.localizedDescription)",
                    preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                return
            }
            self.addRepositoriesFromWrapper(moreWrapper)
            self.isLoadingRepositories = false
            self.tableview?.reloadData()
        })
    }
    
    /* Add recently loaded repositories from the wrapper to our repositories list
     */
    func addRepositoriesFromWrapper(wrapper: RepositoryWrapper?) {
        self.repositoryWrapper = wrapper
        if self.repositories == nil {
            self.repositories = self.repositoryWrapper?.repositories
        } else if let newRepositories = self.repositoryWrapper?.repositories, let currentRepositories = self.repositories {
            self.repositories = currentRepositories + newRepositories
        }
    }
    
}
