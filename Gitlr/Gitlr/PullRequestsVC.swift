//
//  PullRequestsVC.swift
//  Gitlr
//
//  Created by Paulo Moreno on 26/08/16.
//  Copyright © 2016 Paulo Moreno. All rights reserved.
//

import UIKit
import Alamofire

class PullRequestsVC: UITableViewController {
    //
    var pullRequests:Array<PullRequest>?
    var pullRequestsURL:String?
    
    var dateFormatter = NSDateFormatter()
    
    @IBOutlet weak var tableview: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()        

        // Load all pull requests
        self.loadPullRequests()
        
        // Init date formatter (to display the date in a pretty way)
        dateFormatter.dateStyle = NSDateFormatterStyle.LongStyle
        dateFormatter.timeStyle = .MediumStyle
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let pullRequests = self.pullRequests else {
            return 0
        }
        return pullRequests.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // Custom cell for pull requests
        let cell = tableView.dequeueReusableCellWithIdentifier("PullRequestCell", forIndexPath: indexPath) as! PullRequestCell
        
        if let numberOfPullRequests = self.pullRequests?.count where numberOfPullRequests >= indexPath.row {
            if let pullRequest = self.pullRequests?[indexPath.row] {
                
                // Set the cell attributes
                cell.lblPRTitle.text = pullRequest.title
                cell.lblPRBody.text = pullRequest.body
                if let date = pullRequest.date {
                    cell.lblPRDate.text = dateFormatter.stringFromDate(date)
                }
                
                cell.lblAuthorName.text = pullRequest.authorName
                
                // Get the image asynchronous
                if (pullRequest.authorImageURL != nil){
                    Alamofire.request(.GET, pullRequest.authorImageURL!).response { (request, response, data, error) in
                        cell.imgAuthorPicture.image = UIImage(data: data!, scale:1)
                    }
                }
            }
        }
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        // On click, open the browser with the pull request url
        if let pullRequest = self.pullRequests?[indexPath.row], let url = pullRequest.url{
            UIApplication.sharedApplication().openURL(NSURL(string: url)!)
        }
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    // Load all pull requests
    func loadPullRequests() {
        
        PullRequest.getPullRequests(self.pullRequestsURL!, completionHandler: { wrapper, error in
            guard error == nil else {
                // TODO: improved error handling
                let alert = UIAlertController(title: "Error",
                    message: "Could not load first pull requests \(error?.localizedDescription)",
                    preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                return
            }
            self.pullRequests = wrapper?.PullRequests
            self.tableview?.reloadData()
        })
    }

}
