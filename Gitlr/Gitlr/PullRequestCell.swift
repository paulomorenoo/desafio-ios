//
//  PullRequestCell.swift
//  Gitlr
//
//  Created by Paulo Moreno on 26/08/16.
//  Copyright © 2016 Paulo Moreno. All rights reserved.
//

import UIKit

class PullRequestCell: UITableViewCell {
    
    @IBOutlet weak var lblPRTitle: UILabel!
    @IBOutlet weak var lblPRBody: UILabel!
    @IBOutlet weak var lblPRDate: UILabel!
    
    @IBOutlet weak var imgAuthorPicture: UIImageView!
    @IBOutlet weak var lblAuthorName: UILabel!
    
}
